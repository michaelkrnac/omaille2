# Omaille2
Omaille2 is an ORACLE Package using UTL_SMTP to send emails from a message queue

# Installation
 - run omaille2.ddl
 - compile backage from omaille2.sql
 - create job to send messages
 
    ```sql
      -- create job to send messages    
      BEGIN
          omaille2.install;
      END;
    ```
 - enable job
 
# Usage
  - Add new emails to the message queue

 ```sql
-- create message text
BEGIN
    omaille2.create_message(
        p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de'
    );
END;

-- create message text 
BEGIN
    omaille2.create_message(
		p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de',
        p_cc          => 'michael@krnac.de',
        p_bcc         => 'michael@krnac.de',
        p_subject     => 'TEST: create message text maxi',
        p_message     => 'MESSAGE TEST: create message text maxi'
    );
END;

-- create message html
BEGIN
    omaille2.create_message(
		p_host			=> 'localhost',
		p_port			=> 25,
        p_sender		=> 'michael@krnac.de',
        p_recipients	=> 'michael@krnac.de',
        p_cc			=> 'michael@krnac.de',
        p_bcc			=> 'michael@krnac.de',
        p_subject		=> 'TEST: create message html maxi',
        p_message		=> '<h1>MESSAGE TEST:</h1> create message text maxi <a href="http://www.google.de">GOOGLE</a>',
        p_mime_type		=> 'text/html; charset=UTF-8'
    );
END;
```

  - wait until the job sends the emails (default every 5 minutes)

/* omaille2.sql
 *
 * Description: Oracle Email SMTP Robosend Package
 *
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

CREATE OR REPLACE PACKAGE omaille2 AS
	PROCEDURE create_message (
        p_host			VARCHAR2,
		p_port			NUMBER,
		p_sender		VARCHAR2,
        p_recipients	VARCHAR2,
        p_cc			VARCHAR2 DEFAULT NULL,
        p_bcc			VARCHAR2 DEFAULT NULL,
        p_subject		VARCHAR2 DEFAULT NULL,
        p_message		CLOB DEFAULT NULL,
        p_mime_type		VARCHAR2 DEFAULT 'text/plain; charset=UTF-8',
        p_priority		VARCHAR2 DEFAULT 3
    );
	PROCEDURE robo_send;
	PROCEDURE send_html_via_smtp(
		p_host			VARCHAR2,
		p_port			NUMBER,
		p_sender		VARCHAR2,
        p_recipients	VARCHAR2,
        p_cc			VARCHAR2 DEFAULT NULL,
        p_bcc			VARCHAR2 DEFAULT NULL,
        p_subject		VARCHAR2 DEFAULT NULL,
        p_message		CLOB,
        p_mime_type		VARCHAR2 DEFAULT 'text/plain; charset=UTF-8'
	);
	PROCEDURE install;
END omaille2;

CREATE OR REPLACE PACKAGE BODY omaille2 AS

    /*
     * create_message
     * create a new message in the message_queue
    */
    PROCEDURE create_message (
        p_host			VARCHAR2,
		p_port			NUMBER,
		p_sender		VARCHAR2,
        p_recipients	VARCHAR2,
        p_cc			VARCHAR2 DEFAULT NULL,
        p_bcc			VARCHAR2 DEFAULT NULL,
        p_subject		VARCHAR2 DEFAULT NULL,
        p_message		CLOB DEFAULT NULL,
        p_mime_type		VARCHAR2 DEFAULT 'text/plain; charset=UTF-8',
        p_priority		VARCHAR2 DEFAULT 3
    ) IS
    BEGIN
        INSERT INTO omaille2_message_queue (
			host,
			port,
            sender,
            recipients,
            cc,
            bcc,
            subject,
            message,
            mime_type,
            priority)
        VALUES (
			p_host,
			p_port,
            p_sender,
            p_recipients,
            p_cc,
            p_bcc,
            p_subject,
            p_message,
            p_mime_type,
            p_priority);
        COMMIT;
    END create_message;   

    /*
     * robo_send
     * loop over the message queue and send each
     * new message using UTL_SMTP
    */
    PROCEDURE robo_send IS
   	     CURSOR mails IS
            SELECT *
		    FROM omaille2_message_queue
		    WHERE status = 'new';
    BEGIN
        FOR mail IN mails LOOP
		    BEGIN
			    send_html_via_smtp(
					p_host => mail.host,
					p_port => mail.port,
				    p_sender => mail.sender,
			    	p_recipients => mail.recipients,
			    	p_cc => mail.cc,
			    	p_bcc => mail.bcc,
			    	p_subject => mail.subject,
			    	p_message => mail.message,
			    	p_mime_type => mail.mime_type
		    	);	

		    	UPDATE omaille2_message_queue
		    	SET status = 'sent',
                    send_date = SYSTIMESTAMP
		    	WHERE message_id = mail.message_id;
		        COMMIT;
		    EXCEPTION WHEN OTHERS THEN
		
		    	UPDATE omaille2_message_queue
			    SET status = 'failed',
                    send_date = NULL
			    WHERE message_id = mail.message_id;
		        COMMIT;
		    END;
	    END LOOP;
    END robo_send;

	PROCEDURE send_html_via_smtp(
		p_host			VARCHAR2,
		p_port			NUMBER,
		p_sender		VARCHAR2,
        p_recipients	VARCHAR2,
        p_cc			VARCHAR2 DEFAULT NULL,
        p_bcc			VARCHAR2 DEFAULT NULL,
        p_subject		VARCHAR2 DEFAULT NULL,
        p_message		CLOB,
        p_mime_type		VARCHAR2 DEFAULT 'text/plain; charset=UTF-8'
	) IS
		smtp_conn utl_smtp.connection;
		l_offset number;
		l_ammount number; 

		CURSOR rcpts_cur IS
			SELECT trim(COLUMN_VALUE) email
				FROM (SELECT p_recipients email FROM dual
				), xmltable(('"' || REPLACE(email, ',', '","') || '"'))
			UNION
			SELECT trim(COLUMN_VALUE) email
				FROM (SELECT p_cc email FROM dual
				), xmltable(('"' || REPLACE(email, ',', '","') || '"'))
			UNION
			SELECT trim(COLUMN_VALUE) email
				FROM (SELECT p_bcc email FROM dual
				), xmltable(('"' || REPLACE(email, ',', '","') || '"'));
	BEGIN
		FOR mailrcpt IN rcpts_cur LOOP
		    BEGIN
				smtp_conn := utl_smtp.open_connection (p_host, p_port);
				utl_smtp.helo (smtp_conn, p_host);
				utl_smtp.mail (smtp_conn, p_sender);
				utl_smtp.rcpt (smtp_conn, mailrcpt.email);
				utl_smtp.open_data (smtp_conn);

				-- write header
				utl_smtp.write_data( smtp_conn,'Date: ' ||	TO_CHAR(SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss') || utl_tcp.crlf);
				utl_smtp.write_data( smtp_conn,'From: ' || p_sender || utl_tcp.crlf);
				utl_smtp.write_data( smtp_conn,'To: ' || REPLACE(p_recipients,',', ';') || utl_tcp.crlf);
				utl_smtp.write_data( smtp_conn,'Cc: ' || REPLACE(p_cc,',',';') || utl_tcp.crlf);
				utl_smtp.write_data( smtp_conn,'Subject: ' || p_subject || utl_tcp.crlf);
				utl_smtp.write_data( smtp_conn,'Content-Type: ' || p_mime_type || utl_tcp.crlf || utl_tcp.crlf);
				
				-- send the email body in 1900 byte chunks to UTL_SMTP 
				l_offset := 1;
				l_ammount := 1900; 
				WHILE l_offset < dbms_lob.getlength(p_message) LOOP
					utl_smtp.write_data(smtp_conn, dbms_lob.substr(p_message,l_ammount,l_offset));
					l_offset := l_offset + l_ammount ;
					l_ammount := least(1900,dbms_lob.getlength(p_message) - l_ammount);
				end loop; 

				utl_smtp.close_data (smtp_conn);

				utl_smtp.quit (smtp_conn);
			END;
		END LOOP;
	END send_html_via_smtp;

	PROCEDURE install IS
	BEGIN
		/*
		 * create a scheduled job to send out
		 * all new messages in the message queue
	    */
		DBMS_SCHEDULER.CREATE_JOB (
			job_name           => 'omaille2_robo_send',
			job_type           => 'STORED_PROCEDURE',
			job_action         => 'omaille2.robo_send',
			repeat_interval    => 'FREQ=MINUTELY;INTERVAL=5', /* every 5 minutes */
			comments           => 'Run robo_send to send all new messages in the message_queue');    
	END install;
END omaille2;

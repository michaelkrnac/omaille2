/* omaille2_test.sql
 *
 * Description: Test Data for package omaille2
 * 
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

-- create message text mini
BEGIN
    omaille2.create_message(
        p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de'
    );
END;

-- create message text maxi
BEGIN
    omaille2.create_message(
		p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de',
        p_cc          => 'michael@krnac.de',
        p_bcc         => 'michael@krnac.de',
        p_subject     => 'TEST: create message text maxi',
        p_message     => 'MESSAGE TEST: create message text maxi'
    );
END;

-- create message html maxi
BEGIN
    omaille2.create_message(
		p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de',
        p_cc          => 'michael@krnac.de',
        p_bcc         => 'michael@krnac.de',
        p_subject     => 'TEST: create message html maxi',
        p_message     => '<h1>MESSAGE TEST:</h1> create message text maxi <a href="http://www.google.de">GOOGLE</a>',
        p_mime_type   => 'text/html; charset=UTF-8'
    );
END;

-- create message html maxi with multiple emails recipients
BEGIN
    omaille2.create_message(
		p_host		=> 'localhost',
		p_port		=> 25,
        p_sender      => 'michael@krnac.de',
        p_recipients  => 'michael@krnac.de, michael@krnac.de',
        p_cc          => 'michael@krnac.de, michael@krnac.de',
        p_bcc         => 'michael@krnac.de',
        p_subject     => 'TEST: create message html maxi',
        p_message     => '<h1>MESSAGE TEST:</h1> create message text maxi <a href="http://www.google.de">GOOGLE</a>',
        p_mime_type   => 'text/html; charset=UTF-8'
    );
END;

-- send messages manual   
BEGIN
    omaille2.robo_send;
END;

-- install job to send messages    
BEGIN
    omaille2.install;
END;
